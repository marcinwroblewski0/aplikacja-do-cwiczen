<?php

    require "db.php";
    require "response_template.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username"));
        exit();
    }

    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);

    $username = $_POST["user"];
    $username = $db->real_escape_string($username);

    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $sql = "SELECT COUNT(id) AS id_count FROM users WHERE name='$username';";
    
    if(!$result = $db->query($sql)) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
        exit();
    } else {
        while($row = $result->fetch_assoc()) {
            if($row['id_count'] != 0) {
                http_response_code(409);
                echo(ErrorResponseTemplate::createJSONMessage("Username already in use"));
            } else {
                echo(SuccessResponseTemplate::createJSONMessage("Username is free to use"));
            }
        }
        exit();
    }

?>