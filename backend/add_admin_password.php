<?php

    require "db.php";
    require "response_template.php";
    require "auth_admin.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["pass"]) || !isset($_POST["new_pass"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required pass and new pass"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $pass = $_POST["pass"];
    $pass = $db->real_escape_string($pass);
    $newPass = $_POST["new_pass"];
    $newPass = $db->real_escape_string($newPass);
    
    if(strlen($newPass) < 6) {
        http_response_code(403);
        echo(ErrorResponseTemplate::createJSONMessage("Password need to be at least 7 chars"));
        exit();
    }

    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new AdminAuth($pass);

    try {
        if($auth->verify()) {

            addNewPassword($db, $newPass);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function addNewPassword($db, $newPass) {
        $newHashPass = password_hash($newPass, PASSWORD_DEFAULT);
        $sql = "INSERT INTO admin_access (password_hash) VALUES ('$newHashPass')";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            echo(SuccessResponseTemplate::createJSONMessage("Password added"));
        }

    }
?>