<?php

    require "db.php";
    require "response_template.php";
    require "auth_user.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"]) || !isset($_POST["group_number"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username, pin and group number"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $user = $_POST["user"];
    $user = $db->real_escape_string($user);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);
    $group_number = $_POST["group_number"];
    $group_number = filter_var($group_number, FILTER_VALIDATE_INT);
    
    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new Auth($user, $pin);

    try {
        if($auth->verify()) {

            getAnswers($db, $user, $group_number);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function getAnswers($db, $user, $group_number) {

        $master_user_id;

        $sql = "SELECT id FROM users WHERE name LIKE '$user'";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            $master_user_id = $row["id"];
        }

        $sql = "SELECT ok_answers, bad_answers, top_ok, top_bad, game_state FROM groups
        WHERE master_user_id = $master_user_id AND group_number = $group_number";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            
            $row = $result->fetch_assoc();
            if($row != null) {
                echo(SuccessResponseTemplate::createJSONMessage(json_encode($row)));
            } else {
                echo(ErrorResponseTemplate::createJSONMessage("Niepoprawny numer grupy. Być może Twoja grupa została usunięta podczas rozgrywki."));
            }
            exit();
        }

    }


?>