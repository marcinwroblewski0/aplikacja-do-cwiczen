<?php

    require "db.php";
    require "response_template.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username and pin"));
        exit();
    }

    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);

    $username = $_POST["user"];
    $username = $db->real_escape_string($username);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);


    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $sql = "SELECT COUNT(id) AS id_count FROM users WHERE name='$username';";
    
    if(!$result = $db->query($sql)) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
        exit();
    } else {
        while($row = $result->fetch_assoc()) {
            if($row['id_count'] != 0) {
                echo(ErrorResponseTemplate::createJSONMessage("Nazwa użytkownika jest już zajęta"));
            } else {
                if(strlen($pin) != 4) {
                    echo(ErrorResponseTemplate::createJSONMessage("PIN musi mieć dokładnie 4 cyfry"));
                } else if (strlen($username) < 6) {
                    echo(ErrorResponseTemplate::createJSONMessage("Nazwa użytkownika powinna mieć co najmniej 6 znaków!"));
                } else {
                    createUser($db, $username, $pin);
                }
            }
        }
    }


    function createUser($db, $user, $pin) {
        $pin_hash = password_hash($pin, PASSWORD_DEFAULT);

        $sql = "INSERT INTO users (name, pin) VALUES ('$user', '$pin_hash')";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            if($result) {
                echo(SuccessResponseTemplate::createJSONMessage("Account $user created!"));
            }
        }
    }

?>