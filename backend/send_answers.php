<?php

    require "db.php";
    require "response_template.php";
    require "auth_user.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"]) || !isset($_POST["group_number"]) || !isset($_POST["ok_answers"]) || !isset($_POST["bad_answers"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username, pin and answers"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $user = $_POST["user"];
    $user = $db->real_escape_string($user);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);
    $group_number = $_POST["group_number"];
    $group_number = filter_var($group_number, FILTER_VALIDATE_INT);
    $ok_answers = json_decode($_POST["ok_answers"]);

    if(json_last_error() != JSON_ERROR_NONE) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("JSON error while decoding ok_answers: ".json_last_error()));
        exit();
    }
    $bad_answers = json_decode($_POST["bad_answers"]);
    if(json_last_error() != JSON_ERROR_NONE) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("JSON error while decoding bad_answers: ".json_last_error()));
        exit();
    }

    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new Auth($user, $pin);

    try {
        if($auth->verify()) {

            addAnswers($db, $user, $group_number, $ok_answers, $bad_answers);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function addAnswers($db, $user, $group_number, $ok_answers, $bad_answers) {

        $master_user_id;
        $ok_answers_string = $db->real_escape_string(json_encode($ok_answers, JSON_UNESCAPED_SLASHES));
        $bad_answers_string = $db->real_escape_string(json_encode($bad_answers, JSON_UNESCAPED_SLASHES));

        $sql = "SELECT id FROM users WHERE name LIKE '$user'";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            $master_user_id = $row["id"];
        }

        $sql = "UPDATE groups 
        SET ok_answers = '$ok_answers_string', bad_answers = '$bad_answers_string', game_state = 2 
        WHERE master_user_id = '$master_user_id' AND group_number = '$group_number';";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            echo(SuccessResponseTemplate::createJSONMessage("Answers successfully submitted for group number $group_number at user $user"));
            exit();
        }

    }

    function convertArrayToString($array) {
        $response = "";
        foreach ($array as $value) {
            $response.=$value." | ";
        }
        return $response;
    }
?>