<?php

    require "../db.php";
    require "../response_template.php";
    require "../auth_admin.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["pass"]) || !isset($_POST["content"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username and question content"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    if (!$db->set_charset("utf8")) {
        echo(ErrorResponseTemplate::createJSONMessage("Cannot set utf8 character set for communication with database: ".$db->error));
        exit();
    }
    $pass = $_POST["pass"];
    $pass = $db->real_escape_string($pass);
    $content = $_POST["content"];
    $content = $db->real_escape_string($content);
    
    
    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new AdminAuth($pass);

    try {
        if($auth->verify()) {

            deleteQuestion($db, $content);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function deleteQuestion($db, $content) {


        $sql = "DELETE FROM questionnaire_questions WHERE content LIKE '$content';";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            echo(SuccessResponseTemplate::createJSONMessage("Question removed successfully"));
            exit();
        }

    }
?>