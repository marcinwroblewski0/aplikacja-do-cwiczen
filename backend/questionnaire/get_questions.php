<?php

    require "../db.php";
    require "../response_template.php";
    require "../auth_user.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username and pin"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    if (!$db->set_charset("utf8")) {
        echo(ErrorResponseTemplate::createJSONMessage("Cannot set utf8 character set for communication with database: ".$db->error));
        exit();
    }
    $user = $_POST["user"];
    $user = $db->real_escape_string($user);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);
    
    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new Auth($user, $pin);

    try {
        if($auth->verify()) {

            getAllQuestions($db);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function getAllQuestions($db) {
        $sql = "SELECT id, content FROM questionnaire_questions";

        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $responseArray = array();
            while($row = $result->fetch_assoc()) {
                $assoc = array("id" => $row["id"], "content" => $row["content"]);
                array_push($responseArray, $assoc);
            }
        }


        echo(SuccessResponseTemplate::createJSONMessage(json_encode($responseArray)));

    }
?>