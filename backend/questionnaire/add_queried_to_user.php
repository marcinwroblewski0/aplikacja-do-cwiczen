<?php

    require "../db.php";
    require "../response_template.php";
    require "../auth_user.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username and pin"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $username = $_POST["user"];
    $username = $db->real_escape_string($username);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);
    
    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new Auth($username, $pin);
    

    try {
        if($auth->verify()) {

            addGroupToUser($db, $username);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials: ".$username." | $pin"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function addGroupToUser($db, $user) {

        $user_id;
        $queried_already_attached_to_user = 0;

        $sql = "SELECT id FROM users WHERE name LIKE '$user';";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            $user_id = $row["id"];
        }

        $sql = "SELECT COUNT(id) AS id_count FROM queried WHERE master_user_id = $user_id;";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            $queried_already_attached_to_user = $row["id_count"] + 1;
        }


        $sql = "INSERT INTO queried (master_user_id, queried_number) VALUES ($user_id, $queried_already_attached_to_user);";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $responseArray = array('queried_id' => $queried_already_attached_to_user);
            echo(SuccessResponseTemplate::createJSONMessage(json_encode($responseArray)));
        }
    }
?>