<?php

    require "../db.php";
    require "../response_template.php";
    require "../auth_user.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"]) || !isset($_POST["queried_number"]) || !isset($_POST["answers"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username, pin and answers"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $user = $_POST["user"];
    $user = $db->real_escape_string($user);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);
    $queried_number = $_POST["queried_number"];
    $queried_number = filter_var($queried_number, FILTER_VALIDATE_INT);
    $answers = json_decode($_POST["answers"]);

    if(json_last_error() != JSON_ERROR_NONE) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("JSON error while decoding answers: ".json_last_error()));
        exit();
    }

    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new Auth($user, $pin);

    try {
        if($auth->verify()) {

            addAnswers($db, $user, $queried_number, $answers);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function addAnswers($db, $user, $queried_number, $answers) {

        $master_user_id;
        $answers_string = $db->real_escape_string(json_encode($answers, JSON_UNESCAPED_SLASHES));

        $sql = "SELECT id FROM users WHERE name LIKE '$user'";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            $master_user_id = $row["id"];
        }


        $sql = "SELECT id FROM queried WHERE master_user_id = $master_user_id AND queried_number = $queried_number";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            if(count($row) > 0) {
                $sql = "UPDATE queried 
                SET answers = '$answers_string' 
                WHERE master_user_id = '$master_user_id' AND queried_number = '$queried_number';";
                if(!$result = $db->query($sql)) {
                    http_response_code(500);
                    echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
                    exit();
                } else {
                    echo(SuccessResponseTemplate::createJSONMessage("Answers successfully submitted for queried number $queried_number at user $user"));
                    exit();
                }
            } else {

                $sql = "INSERT INTO queried 
                (master_user_id, queried_number, answers) VALUES ($master_user_id, $queried_number, '$answers_string');";
                if(!$result = $db->query($sql)) {
                    http_response_code(500);
                    echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
                    exit();
                } else {
                    echo(SuccessResponseTemplate::createJSONMessage("Answers successfully submitted for queried number $queried_number at user $user"));
                    exit();
                }
            }
        }



        

    }

    function convertArrayToString($array) {
        $response = "";
        foreach ($array as $value) {
            $response.=$value." | ";
        }
        return $response;
    }
?>