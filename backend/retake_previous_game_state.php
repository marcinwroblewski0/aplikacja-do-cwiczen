<?php

    require "db.php";
    require "response_template.php";
    require "auth_user.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"]) || !isset($_POST["group_number"]) || !isset($_POST["current_game_state"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username, pin, group number and current game state"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $user = $_POST["user"];
    $user = $db->real_escape_string($user);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);
    $group_number = $_POST["group_number"];
    $group_number = filter_var($group_number, FILTER_VALIDATE_INT);
    $current_game_state = $_POST["current_game_state"];
    $current_game_state = filter_var($current_game_state, FILTER_VALIDATE_INT);
    
    if($current_game_state <= 1 || $current_game_state > 4) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Current game state cannot be lower/equal to 1 and higher than 4"));
        exit();
    }

    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new Auth($user, $pin);

    try {
        if($auth->verify()) {

            retake($db, $user, $group_number, $current_game_state);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function retake($db, $user, $group_number, $current_game_state) {

        $master_user_id;

        $sql = "SELECT id FROM users WHERE name LIKE '$user'";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            $master_user_id = $row["id"];
        }

        $previous_game_state = $current_game_state - 1;

        $sql = "UPDATE groups SET game_state = $previous_game_state
        WHERE master_user_id = $master_user_id AND group_number = $group_number";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            
            echo(SuccessResponseTemplate::createJSONMessage("Stan gry został cofnięty do poprzedniego stanu: $previous_game_state"));
            
            exit();
        }

    }


?>