<?php

    class Auth {

        private $_user;
        private $_pin;

        public function __construct($username, $pass) {
            $this->_user = $username;
            $this->_pin = $pass;
        }

        private function isUserInDatabase($username) {
            
            require "db.php";

            $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
           
            if($db->connect_errno > 0) {
                throw new Exception("Cannot connect to the database");
                return false;
            }

            $sql = "SELECT COUNT(id) AS id_count FROM users WHERE name='$username';";
            
            if(!$result = $db->query($sql)) {
                throw new Exception("Cannot run the query on database: ".$db->error);
                return false;
            } else {
                while($row = $result->fetch_assoc()) {
                    if($row['id_count'] == 1) {
                        return true;
                    } else {
                        throw new Exception("There's no $username in the database");
                    }
                }
            }


        }

        private function isPinCorrect($pin) {

            require "db.php";

            $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
            if($db->connect_errno > 0) {
                throw new Exception("Cannot connect to the database");
                return false;
            }

            $sql = "SELECT pin FROM users WHERE name='$this->_user';";
            
            if(!$result = $db->query($sql)) {
                throw new Exception("Cannot run the query on database: ".$db->error);
                return false;
            } else {
                while($row = $result->fetch_assoc()) {
                    if(password_verify($pin, $row["pin"])) {
                        return true;
                    } else {
                        throw new Exception("PIN incorrect");
                    }
                }
            }

        }

        public function verify() {
            return $this->isUserInDatabase($this->_user) && $this->isPinCorrect($this->_pin);

        }
    }


?>