<?php


    class AdminAuth {

        private $_pin;

        public function __construct($pass) {
            $this->_pin = $pass;
        }


        private function isPinCorrect($pin) {

            require "db.php";

            $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
            if($db->connect_errno > 0) {
                throw new Exception("Cannot connect to the database");
                return false;
            }

            $sql = "SELECT password_hash FROM admin_access;";
            
            if(!$result = $db->query($sql)) {
                throw new Exception("Cannot run the query on database: ".$db->error);
                return false;
            } else {
                $is_password_ok = false;
                while($row = $result->fetch_assoc()) {
                    if(password_verify($pin, $row["password_hash"])) {
                        $is_password_ok = true;
                    } 
                }

                if(!$is_password_ok){
                    throw new Exception("PIN incorrect");
                }

                return $is_password_ok;
            }

        }

        public function verify() {
            return $this->isPinCorrect($this->_pin);

        }
    }


?>