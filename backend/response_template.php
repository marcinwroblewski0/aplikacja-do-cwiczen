<?php

    class ErrorResponseTemplate {
        public static function createJSONMessage($message) {
            json_decode($message);
            if(json_last_error() == 0) {
                $jsonAssoc = json_decode($message);
                $responseArray = array("status" => "error", "message" => json_encode($jsonAssoc));
                return json_encode($responseArray);
            } else {
                $responseArray = array("status" => "error", "message" => $message);
                return json_encode($responseArray);
            }
        }
    }

    class SuccessResponseTemplate {
        public static function createJSONMessage($message) {
            json_decode($message);
            if(json_last_error() == 0) {
                $jsonAssoc = json_decode($message);
                $responseArray = array("status" => "success", "message" => json_encode($jsonAssoc));
                return json_encode($responseArray);
            } else {
                $responseArray = array("status" => "success", "message" => $message);
                return json_encode($responseArray);
            }
            
        }
    }

?>