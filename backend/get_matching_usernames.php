<?php

    require "db.php";
    require "response_template.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required username"));
        exit();
    }

    

    if(strlen($username) < 5) {
        echo(ErrorResponseTemplate::createJSONMessage("Username is not long enough to process: Got ".strlen($username). " ($username)"));
        exit();
    }

    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $username = $_POST["user"];
    $username = $db->real_escape_string($username);
    
    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $sql = "SELECT name FROM users WHERE name LIKE '$username%';";
    
    if(!$result = $db->query($sql)) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
        exit();
    } else {
        $responseArray = array();
        while($row = $result->fetch_assoc()) {
            array_push($responseArray, $row["name"]);
        }

        if(count($responseArray) == 0) {
            echo(ErrorResponseTemplate::createJSONMessage("No matching usernames"));
        }
        echo(SuccessResponseTemplate::createJSONMessage(json_encode($responseArray)));
        exit();
    }

?>