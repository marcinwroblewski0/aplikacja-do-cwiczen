<?php

    require "db.php";
    require "response_template.php";
    require "auth_user.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["user"]) || !isset($_POST["pin"]) || !isset($_POST["group_number"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required user, pin and group_number"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $username = $_POST["user"];
    $username = $db->real_escape_string($username);
    $pin = $_POST["pin"];
    $pin = filter_var($pin, FILTER_VALIDATE_INT);
    $groupNumber = $_POST["group_number"];
    $groupNumber = filter_var($groupNumber, FILTER_VALIDATE_INT);
    
    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }

    $auth = new Auth($username, $pin);

    try {
        if($auth->verify()) {

            removeGroupFromUser($db, $username, $groupNumber);

        } else {
            http_response_code(401);
            echo(ErrorResponseTemplate::createJSONMessage("Wrong credentials: ".$username." | $pin"));
            exit();
        }
        
    } catch (Exception $e) {
        http_response_code(401);
        echo(ErrorResponseTemplate::createJSONMessage($e->getMessage()));
        exit();
    }

    function removeGroupFromUser($db, $user, $groupNumber) {

        $user_id;

        $sql = "SELECT id FROM users WHERE name LIKE '$user';";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            $row = $result->fetch_assoc();
            $user_id = $row["id"];
        }

        $sql = "DELETE FROM groups WHERE master_user_id = $user_id AND group_number = $groupNumber;";
        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            echo(SuccessResponseTemplate::createJSONMessage("Group $groupNumber removed from $user"));
        }

    }
?>