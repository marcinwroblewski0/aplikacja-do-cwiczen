<?php

    require "db.php";
    require "response_template.php";
    require "auth_admin.php";

    header('Access-Control-Allow-Origin: *');  
    header('Content-Type: text/html; charset=utf-8');

    if(!isset($_POST) || !isset($_POST["sql"])) {
        http_response_code(400);
        echo(ErrorResponseTemplate::createJSONMessage("Required sql"));
        exit();
    }


    $db = new mysqli($db_address, $db_user, $db_pass, $db_name);
    $sql = $_POST["sql"];

    if($db->connect_errno > 0) {
        http_response_code(500);
        echo(ErrorResponseTemplate::createJSONMessage("Cannot connect to database: ".$db->error));
        exit();
    }


    runQuery($sql, $db);


    function runQuery($sql, $db) {

        if(!$result = $db->query($sql)) {
            http_response_code(500);
            echo(ErrorResponseTemplate::createJSONMessage("Cannot run the query on database: ".$db->error));
            exit();
        } else {
            echo(var_dump($result));
            echo(var_dump($result->fetch_assoc()));

            $responseArray = array();
            while($row = $result->fetch_assoc()) {
                array_push($responseArray, $row["content"]);
            }
            echo(var_dump($responseArray));
        }

    }
?>