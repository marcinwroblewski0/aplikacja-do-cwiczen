function redirectToCorrectPlace(currentGameState) {
    $.post(apiRoot + "get_all_answers_for_group.php",
    {"user": getItem("user"), "pin": getItem("pin"), "group_number": getItem("group_id")})
    .done(function(reponse) {

        var data = JSON.parse(reponse);
        if(data.status == "success") {
            var message = JSON.parse(data.message);
            if(parseInt(message.game_state) != parseInt(currentGameState)) {
                redirectTo(message.game_state);
            } 
        } else {
            window.location.replace("index.html");
        }

    })
    .fail(function(err) {
        window.location.replace("index.html");
    });
}

function redirectTo(gameState) {
    switch(parseInt(gameState)) {
        case 1:
            window.location.replace("cards.html"); break;
            
        case 2:
            window.location.replace("top3_ok.html"); break;
            
        case 3:
            window.location.replace("top3_bad.html"); break;
            
        case 4:
            window.location.replace("results.html"); break;
    }
}


function redirectToHome() {
    window.location.replace("../index.html");
}

function retakePreviousGameState(currentGameState) {
    $.post(apiRoot + "retake_previous_game_state.php",
    {"user": getItem("user"), "pin": getItem("pin"), "group_number": getItem("group_id"), "current_game_state": currentGameState})
    .done(function(reponse) {

        var data = JSON.parse(reponse);
        if(data.status == "success") {
            redirectTo(parseInt(currentGameState) - 1);
        } else {
            window.location.replace("index.html");
        }

    })
    .fail(function(err) {
        window.location.replace("index.html");
    });
}

$('a').click(function() {
            window.location.href = this.href;
            return false;
        });